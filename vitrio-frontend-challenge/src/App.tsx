import React from 'react';
import {ProductList} from "./components/ProductList";
import './styles/App.scss';
import {Newsletter} from "./components/Newsletter";

function App() {
    return (
        <div className="App">
            <ProductList/>
            <Newsletter/>
        </div>
    );
}

export default App;
