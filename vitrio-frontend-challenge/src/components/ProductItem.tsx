import '../styles/ProductItem.scss';

interface  ProductItemProps {
    product: {
        productName: string;
        items: [{
            images: [{
                imageUrl: string;
                imageText: string;
            }]
            sellers: [{
                commertialOffer: {
                    Price: number;
                    AvailableQuantity: number;
                }
            }]
        }]
    }
}

export function ProductItem(props: ProductItemProps) {

    function currencyFormat(num: any) {
        num = num.toFixed(2).replace(".", ",");
        return 'R$' + num.replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
    }

    function divideValue(num: number, divider: number) {
        return currencyFormat(num / divider);
    }

    const verifyAvailableQuantity = () => {
        if (props.product.items[0].sellers[0].commertialOffer.AvailableQuantity > 0) {
            return (
                <>
                    <strong className='product-price'>{currencyFormat(props.product.items[0].sellers[0].commertialOffer.Price)}</strong>
                    <span className='product-discount'>10x de {divideValue(props.product.items[0].sellers[0].commertialOffer.Price, 10)} sem juros</span>
                    <button className='product-button'>comprar</button>
                </>
            )
        } else {
            return <button className='product-button button-consult'>consulte</button>;
        }
    }

    return (
        <li className='product-item'>
            <img className='product-img' alt={props.product.items[0].images[0].imageText} src={props.product.items[0].images[0].imageUrl}/>
            <strong className='product-name'>{props.product.productName}</strong>
            {verifyAvailableQuantity()}
        </li>
    )
}