import React from "react";
import '../styles/Newsletter.scss';


export class Newsletter extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            name: '',
            email: '',
            isActive: false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    cleanForm = () => {
        this.setState({
            name: '',
            email: '',
        })
    }

    handleShow = () => {
        this.setState({isActive: true});
    };

    handleHide = () => {
        this.setState({isActive: false});
    };

    handleChange(event: { target: { name: string; value: string; }; }) {
        this.setState({[event.target.name]: (event.target.value)});
    }

    handleSubmit(event: { preventDefault: () => void; }) {
        event.preventDefault();
        this.cleanForm();
        this.handleShow();
        setTimeout(this.handleHide, 3000);
    }

    render() {
        return (
            <div className='newsletter-div'>
                <h2 className='newsletter-title'>assine nossa newsletter</h2>
                <span className='newsletter-subtitle'>Fique por dentro das nossas novidade e receba 10% de desconto na primeira compra.</span>
                <span className='newsletter-text'>* válido somente para jóias e não acumulativo com outras promoções</span>
                <form className='newsletter' onSubmit={this.handleSubmit}>
                    <input required name='name' className='newsletter-input' type='text' placeholder='Seu nome' value={this.state.name} onChange={this.handleChange}/>
                    <input required name='email' className='newsletter-input' type='email' placeholder='Seu e-mail' value={this.state.email} onChange={this.handleChange}/>
                    <button type='submit' className='newsletter-submit'>enviar</button>
                </form>
                {this.state.isActive ?(
                    <label className='newsletter-helper'>Inscrição Enviada com Sucesso! Em breve retornaremos seu contato!</label>
                ) : ''}
            </div>
        )
    }
}