import {useEffect, useState} from "react";
import {ProductItem} from "./ProductItem";
import '../styles/ProductList.scss';

interface Product {
    productId: number;
    productName: string;
    items: [{
        images: [{
            imageUrl: string;
            imageText: string;
        }]
        sellers: [{
            commertialOffer: {
                Price: number;
                AvailableQuantity: number;
            }
        }]
    }]
}

export function ProductList() {
    const [products, setProducts] = useState<Product[]>([]);

    useEffect(() => {
        fetch('https://bitbucket.org/jeef_silva/vitrio-dev-test/raw/master/data/catalogo.json')
            .then(response => response.json())
            .then(data => setProducts(data))
    }, []);

    return (
        <ul className="product-list">
            {products.map(product => {
                return <ProductItem key={product.productId} product={product} />
            })}
        </ul>
    );
}