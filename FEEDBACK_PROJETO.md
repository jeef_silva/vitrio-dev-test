# Feedback do projeto

## O que fiz
- Componentes Catálogo de Produtos e Newsletter, com layout responsivo.

## O que não consegui fazer
- 


## Considerações finais sobre o projeto
Conte para nós aqui o que você sentiu ao fazer o projeto e quais dificuldades teve ao longo do desenvolvimento... <br><br>
Gostei bastante de todas as instruções, foram bem específicas e ajudaram no desenvolvimento do teste. De todos os testes que fiz até hoje, esse foi o melhor explicado e definido. Sobre o desenvolvimento em si, foi bem tranquilo construir, não tive nenhuma dificuldade para desenvolver.